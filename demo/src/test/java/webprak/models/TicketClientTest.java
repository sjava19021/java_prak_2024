package webprak.models;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TicketClientTest {
    @Test
    void ticketCkientTest(){
        TicketClient ticketClient = new TicketClient();
        ticketClient.setId(1L);
        ticketClient.setTicketId(1L);
        ticketClient.setClientId(1L);

        assertEquals(ticketClient.getId(), 1L);
        assertEquals(ticketClient.getTicketId(), 1L);
        assertEquals(ticketClient.getClientId(), 1L);
    }
}
