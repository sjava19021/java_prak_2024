package webprak.models;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RaceTest {
    @Test
    void raceTest1(){
        Race race = new Race();
        race.setId(1L);
        race.setRaceNumber(10L);
        race.setCompanyName("RZHD");
        race.setSeatsNumber(100L);
        String[] stations = {"Moscow", "Saint-Petersburg", "Novosibirsk"};
        race.setStations(stations);
        Long[] costs = {0L, 200L, 500L};
        race.setCosts(costs);
        Long[] time = {0L, 1000L, 10000L};
        race.setTime(time);
        race.setDateFrom(123L);
        race.setDateTo(456L);

        assertEquals(race.getId(), 1L);
        assertEquals(race.getRaceNumber(), 10L);
        assertEquals(race.getCompanyName(), "RZHD");
        assertEquals(race.getSeatsNumber(), 100L);
        assertEquals(race.getStations(), stations);
        assertEquals(race.getCosts(), costs);
        assertEquals(race.getTime(), time);
        assertEquals(race.getDateFrom(), 123L);
        assertEquals(race.getDateTo(), 456L);
    }

    @Test
    void raceTest2(){
        String[] stations = {"Moscow", "Saint-Petersburg", "Novosibirsk"};
        Long[] costs = {0L, 200L, 500L};
        Long[] time = {0L, 1000L, 10000L};
        Race race = new Race(1L, 10L, "RZHD", 100L, stations, costs, time, 123L, 456L);


        assertEquals(race.getId(), 1L);
        assertEquals(race.getRaceNumber(), 10L);
        assertEquals(race.getCompanyName(), "RZHD");
        assertEquals(race.getSeatsNumber(), 100L);
        assertEquals(race.getStations().length, stations.length);
        for(int i = 0; i < race.getStations().length; ++i){
            assertEquals(race.getStations()[i], stations[i]);
        }

        for(int i = 0; i < race.getCosts().length; ++i){
            assertEquals(race.getCosts()[i], costs[i]);
        }

        for(int i = 0; i < race.getTime().length; ++i){
            assertEquals(race.getTime()[i], time[i]);
        }
        assertEquals(race.getDateFrom(), 123L);
        assertEquals(race.getDateTo(), 456L);
    }
}
