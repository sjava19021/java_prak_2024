package webprak.models;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TicketTest {
    @Test
    void ticketTest1(){
        Ticket ticket = new Ticket();
        ticket.setId(1L);
        ticket.setRaceId(1L);
        ticket.setDate(0L);
        ticket.setPlaceNumber(40L);
        ticket.setStationFrom("Moscow");
        ticket.setStationTo("Saint-Petersburg");
        ticket.setCosts(100L);

        assertEquals(ticket.getId(), 1L);
        assertEquals(ticket.getRaceId(), 1L);
        assertEquals(ticket.getDate(), 0L);
        assertEquals(ticket.getPlaceNumber(), 40L);
        assertEquals(ticket.getStationFrom(), "Moscow");
        assertEquals(ticket.getStationTo(), "Saint-Petersburg");
        assertEquals(ticket.getCosts(), 100L);
    }

    @Test
    void ticketTest2(){
        Ticket ticket = new Ticket(1L, 1L, 0L, 40L, "Moscow", "Saint-Petersburg", 100L);

        assertEquals(ticket.getId(), 1L);
        assertEquals(ticket.getRaceId(), 1L);
        assertEquals(ticket.getDate(), 0L);
        assertEquals(ticket.getPlaceNumber(), 40L);
        assertEquals(ticket.getStationFrom(), "Moscow");
        assertEquals(ticket.getStationTo(), "Saint-Petersburg");
        assertEquals(ticket.getCosts(), 100L);
    }
}
