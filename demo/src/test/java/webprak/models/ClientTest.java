package webprak.models;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ClientTest {
    @Test
    void clientTest1(){
        Client client = new Client();
        client.setId(1L);
        client.setName("Ivan");
        client.setSurname("Ivanov");
        client.setPatronymic("Ivanovich");
        client.setAddress("Moscow");
        client.setEmail("ivan@ivan.ru");

        assertEquals(client.getId(), 1L);
        assertEquals(client.getName(), "Ivan");
        assertEquals(client.getSurname(), "Ivanov");
        assertEquals(client.getPatronymic(), "Ivanovich");
        assertEquals(client.getAddress(), "Moscow");
        assertEquals(client.getEmail(), "ivan@ivan.ru");
    }

    @Test
    void clientTest2(){
        Client client = new Client(1L, "Ivan", "Ivanov", "Ivanovich", "Moscow", "Russia", "ivan@ivan.ru", "", false);

        assertEquals(client.getId(), 1L);
        assertEquals(client.getName(), "Ivan");
        assertEquals(client.getSurname(), "Ivanov");
        assertEquals(client.getPatronymic(), "Ivanovich");
        assertEquals(client.getAddress(), "Moscow");
        assertEquals(client.getEmail(), "ivan@ivan.ru");
    }
}