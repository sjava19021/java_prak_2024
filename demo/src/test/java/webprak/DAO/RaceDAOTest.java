package webprak.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import webprak.models.Race;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application.properties")
public class RaceDAOTest {
    @Autowired
    private RaceDAO raceDAO;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    void testSimpleManipulations(){
        //test corectness of inserting

        List<Race> races = (List<Race>) raceDAO.getAll();
        assertEquals(races.size(), 3);

        for(Long i = 1L; i <= races.size(); ++i){
            Race raceById = raceDAO.getById(i);
            assertNotNull(raceById);
            assertEquals(raceById.getId(), i);
        }

        Race raceNotExist = raceDAO.getById(100L);
        assertNull(raceNotExist);

        //test update
        Race updatedRace = new Race(1L, 10L, "RZHD", 100L, new String[]{"Moscow", "Saint-Petersburg", "Novosibirsk"}, new Long[]{0L, 200L, 500L}, new Long[]{0L, 1000L, 10000L}, 123L, 456L);
        raceDAO.update(updatedRace);
        assertEquals(raceDAO.getAll().size(), 3);


        assertEquals(raceDAO.getById(1L).getRaceNumber(), 10L);
        assertEquals(raceDAO.getById(1L).getCompanyName(), "RZHD");
        assertEquals(raceDAO.getById(1L).getSeatsNumber(), 100L);
        assertEquals(raceDAO.getById(1L).getStations()[0], "Moscow");
        assertEquals(raceDAO.getById(1L).getStations()[1], "Saint-Petersburg");
        assertEquals(raceDAO.getById(1L).getStations()[2], "Novosibirsk");
        assertEquals(raceDAO.getById(1L).getCosts()[0], 0L);
        assertEquals(raceDAO.getById(1L).getCosts()[1], 200L);
        assertEquals(raceDAO.getById(1L).getCosts()[2], 500L);
        assertEquals(raceDAO.getById(1L).getTime()[0], 0L);
        assertEquals(raceDAO.getById(1L).getTime()[1], 1000L);
        assertEquals(raceDAO.getById(1L).getTime()[2], 10000L);
        assertEquals(raceDAO.getById(1L).getDateFrom(), 123L);
        assertEquals(raceDAO.getById(1L).getDateTo(), 456L);

        //test delete
        Race raceToDelete = raceDAO.getById(3L);
        assertNotNull(raceToDelete);
        assertEquals(raceToDelete.getId(), 3);
        raceDAO.delete(raceToDelete);

        raceToDelete = raceDAO.getById(3L);
        assertNull(raceToDelete);
    }

    @BeforeEach
    void beforeEach(){
        List<Race> raceList = new ArrayList<>();
        raceList.add(new Race(1L, 11L, "RZHD", 100L, new String[]{"Moscow", "Saint-Petersburg", "Novosibirsk"}, new Long[]{0L, 200L, 500L}, new Long[]{0L, 1000L, 10000L}, 123L, 456L));
        raceList.add(new Race(2L, 12L, "RZHD", 150L, new String[]{"Saint-Petersburg", "Novosibirsk"}, new Long[]{0L, 300L}, new Long[]{0L, 10000L}, 123L, 456L));
        raceList.add(new Race(3L, 13L, "RZHD", 200L, new String[]{"Novosibirsk", "Saint-Petersburg", "Moscow"}, new Long[]{0L, 200L, 500L}, new Long[]{0L, 1000L, 10000L}, 123L, 456L));
        raceDAO.saveCollection(raceList);
    }

    @AfterEach
    void afterEach(){
        Collection<Race> races = raceDAO.getAll();
        for (Race race : races) {
            raceDAO.delete(race);
        }
        assertEquals(raceDAO.getAll().size(), 0);
    }

    @BeforeAll
    @AfterEach
    void startSession(){
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.createSQLQuery("TRUNCATE races RESTART IDENTITY CASCADE").executeUpdate();
            session.getTransaction().commit();
        }
    }
}
