package webprak.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import webprak.models.Client;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application.properties")
public class ClientDAOTest {


    @Autowired
    private ClientDAO clientDAO;
    @Autowired
    private SessionFactory sessionFactory;

    @Test
    void testSimpleManipulations(){
        //test corectness of inserting
        List<Client> clients = (List<Client>) clientDAO.getAll();
        assertEquals(clients.size(), 3);

        for(Long i = 1L; i <= clients.size(); ++i){
            Client clientById = clientDAO.getById(i);
            assertNotNull(clientById);
            assertEquals(clientById.getId(), i);
        }

        Client clientNotExist = clientDAO.getById(100L);
        assertNull(clientNotExist);

        //test update
        Client updatedClient = new Client(1L, "Ivan", "Ivanov", "Ivanovich", "Moscow", "Russia", "new@new.ru",  "qwerty", false);

        clientDAO.update(updatedClient);
        assertEquals(clientDAO.getAll().size(), 3);

        assertEquals(clientDAO.getById(1L).getName(), "Ivan");
        assertEquals(clientDAO.getById(1L).getSurname(), "Ivanov");
        assertEquals(clientDAO.getById(1L).getPatronymic(), "Ivanovich");
        assertEquals(clientDAO.getById(1L).getAddress(), "Moscow");
        assertEquals(clientDAO.getById(1L).getEmail(), "new@new.ru");

        //test delete
        Client clientToDelete = clientDAO.getById(3L);
        assertNotNull(clientToDelete);
        assertEquals(clientToDelete.getId(), 3);
        clientDAO.delete(clientToDelete);

        clientToDelete = clientDAO.getById(3L);
        assertNull(clientToDelete);
    }

    @BeforeEach
    void beforeEach(){
        List<Client> clientList = new ArrayList<>();
        clientList.add(new Client(1L, "Ivan", "Ivanov", "Ivanovich", "Moscow", "Russia", "ivan@ivan.ru", "", false));
        clientList.add(new Client(2L, "Petr", "Petrov", "Petrovich", "Saint-Peterburg", "Russia", "petr@petr.ru", "", false));
        clientList.add(new Client(3L, "Kirill", "Kirillov", "Kirillovich", "Novosibirsk", "Russia", "kirill@kirill.ru", "", false));
        clientDAO.saveCollection(clientList);
    }

    @AfterEach
    void afterEach(){
        var clients = clientDAO.getAll();
        for (Client client : clients) {
            clientDAO.delete(client);
        }
        assertEquals(clientDAO.getAll().size(), 0);
    }

    @BeforeAll
    @AfterEach
    void startSession(){
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.createSQLQuery("TRUNCATE clients RESTART IDENTITY CASCADE").executeUpdate();
            session.getTransaction().commit();
        }
    }
}
