package webprak.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import webprak.models.Race;
import webprak.models.Ticket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application.properties")
public class TicketDAOTest {
    @Autowired
    private TicketDAO ticketDAO;
    @Autowired
    private RaceDAO raceDAO;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    void testSimpleManipulations(){
        //test corectness of inserting

        List<Ticket> tickets = (List<Ticket>) ticketDAO.getAll();
        assertEquals(tickets.size(), 3);

        for(Long i = 1L; i <= tickets.size(); ++i){
            Ticket ticketById = ticketDAO.getById(i);
            assertNotNull(ticketById);
            assertEquals(ticketById.getId(), i);
        }

        Ticket ticketNotExist = ticketDAO.getById(100L);
        assertNull(ticketNotExist);

        //test update
        Ticket updatedTicket = new Ticket(1L, 1L, 0L, 65L, "Saint-Petersburg", "Novosibirsk", 300L);
        ticketDAO.update(updatedTicket);
        assertEquals(ticketDAO.getAll().size(), 3);

        assertEquals(ticketDAO.getById(1L).getId(), 1L);
        assertEquals(ticketDAO.getById(1L).getRaceId(), 1L);
        assertEquals(ticketDAO.getById(1L).getDate(), 0L);
        assertEquals(ticketDAO.getById(1L).getPlaceNumber(), 65L);
        assertEquals(ticketDAO.getById(1L).getStationFrom(), "Saint-Petersburg");
        assertEquals(ticketDAO.getById(1L).getStationTo(), "Novosibirsk");
        assertEquals(ticketDAO.getById(1L).getCosts(), 300L);

        //test delete
        Ticket ticketToDelete = ticketDAO.getById(3L);
        assertNotNull(ticketToDelete);
        assertEquals(ticketToDelete.getId(), 3);
        ticketDAO.delete(ticketToDelete);

        ticketToDelete = ticketDAO.getById(3L);
        assertNull(ticketToDelete);
    }

    @BeforeEach
    void beforeEach(){
        List<Ticket> ticketList = new ArrayList<>();
        List<Race> raceList = new ArrayList<>();
        Race race = new Race(1L, 11L, "RZHD", 100L, new String[]{"Moscow", "Saint-Petersburg", "Novosibirsk"}, new Long[]{0L, 200L, 500L}, new Long[]{0L, 1000L, 10000L}, 123L, 456L);
        raceList.add(race);
        raceDAO.saveCollection(raceList);

        ticketList.add(new Ticket(1L, 1L, 0L, 66L, "Saint-Petersburg", "Novosibirsk", 300L));
        ticketList.add(new Ticket(2L, 1L, 0L, 67L, "Moscow", "Novosibirsk", 500L));
        ticketList.add(new Ticket(3L, 1L, 0L, 68L, "Moscow", "Saint-Petersburg", 200L));
        ticketDAO.saveCollection(ticketList);
    }

    @AfterEach
    void afterEach(){
        Collection<Ticket> tickets = ticketDAO.getAll();
        for (Ticket ticket : tickets) {
            ticketDAO.delete(ticket);
        }
        assertEquals(ticketDAO.getAll().size(), 0);
    }

    @BeforeAll
    @AfterEach
    void startSession(){
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.createSQLQuery("TRUNCATE tickets RESTART IDENTITY CASCADE").executeUpdate();
            session.createSQLQuery("TRUNCATE races RESTART IDENTITY CASCADE").executeUpdate();
            session.getTransaction().commit();
        }
    }
}
