package webprak;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import webprak.DAO.impl.ClientDAOImpl;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WebTest {
    private int timeOut = 1500;
    private final String homeTitle = "homePage";
    private final String searchTitle = "searchPage";
    private final String enryTitle = "entryPage";
    private final String racesTitle = "racesPage";
    private final String registrationTitle = "registrationPage";
    private final String clientsTitle = "clientsPage";
    private final String ticketsTitle = "ticketsPage";
    private final String errorTitle = "errorPage";
    private final String orderTitle = "orderPage";


    @Test
    void MainTest() {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");

        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--remote-allow-origins=*");
        opt.setBinary("/usr/bin/chromium-browser");

        ChromeDriver driver = new ChromeDriver(opt);
        driver.manage().window().setPosition(new Point(0, 0));
        driver.manage().window().setSize(new Dimension(1845, 1200));

        // pages for unlogined users
        driver.get("http://localhost:8080/");
        driver.findElement(By.id("rootLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), homeTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("searchLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), searchTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("racesListLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), racesTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("registrationLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), registrationTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("entryLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), enryTitle);

        //pages for admin (pages : searchLink racesListLink clientsListLink ticketsListLink exitLink)
        driver.get("http://localhost:8080/");
        driver.findElement(By.id("entryLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);

        driver.findElement(By.id("login")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("admin");
        driver.findElement(By.id("enter")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);

        driver.get("http://localhost:8080/");
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), homeTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("searchLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), searchTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("racesListLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), racesTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("clientsListLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), clientsTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("ticketsListLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), ticketsTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("exitLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), homeTitle);

        //logined user
        driver.get("http://localhost:8080/");
        driver.findElement(By.id("entryLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);

        driver.findElement(By.id("login")).sendKeys("sjava19021@gmail.com");
        driver.findElement(By.id("password")).sendKeys("H2002slavik!");
        driver.findElement(By.id("enter")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);

        driver.get("http://localhost:8080/");
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), homeTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("searchLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), searchTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("racesListLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), racesTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("ticketsListLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), ticketsTitle);

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("exitLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), homeTitle);

        driver.quit();
    }

    @Test
    void loginUserTest(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--remote-allow-origins=*");
        opt.setBinary("/usr/bin/chromium-browser");
        ChromeDriver driver = new ChromeDriver(opt);
        driver.manage().window().setPosition(new Point(0, 0));
        driver.manage().window().setSize(new Dimension(1845, 1200));

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("registrationLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), registrationTitle);
        driver.findElement(By.id("name")).sendKeys("name");
        driver.findElement(By.id("surname")).sendKeys("surname");
        driver.findElement(By.id("patronymic")).sendKeys("patronymic");
        driver.findElement(By.id("email")).sendKeys("email@email.ru");
        driver.findElement(By.id("address")).sendKeys("address");
        driver.findElement(By.id("password")).sendKeys("password");
        driver.findElement(By.id("registration")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), enryTitle);

        driver.findElement(By.id("login")).sendKeys("email@email.ru");
        driver.findElement(By.id("password")).sendKeys("password");
        driver.findElement(By.id("enter")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), homeTitle);

        driver.findElement(By.id("exitLink")).click();
        assertEquals(driver.getTitle(), homeTitle);
    }

    @Test
    void badLoginOrPassword(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--remote-allow-origins=*");
        opt.setBinary("/usr/bin/chromium-browser");
        ChromeDriver driver = new ChromeDriver(opt);
        driver.manage().window().setPosition(new Point(0, 0));
        driver.manage().window().setSize(new Dimension(1845, 1200));

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("entryLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        driver.findElement(By.id("login")).sendKeys("bad_login");
        driver.findElement(By.id("password")).sendKeys("bad_password");
        driver.findElement(By.id("enter")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), errorTitle);
    }

    @Test
    void buyTicketTest(){
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("--remote-allow-origins=*");
        opt.setBinary("/usr/bin/chromium-browser");
        ChromeDriver driver = new ChromeDriver(opt);
        driver.manage().window().setPosition(new Point(0, 0));
        driver.manage().window().setSize(new Dimension(1845, 1200));

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("entryLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        driver.findElement(By.id("login")).sendKeys("sjava19021@gmail.com");
        driver.findElement(By.id("password")).sendKeys("H2002slavik!");
        driver.findElement(By.id("enter")).click();

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("racesListLink")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        driver.findElement(By.id("buyButton")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), orderTitle);

        driver.findElement(By.id("placeNumber")).sendKeys("123");
        driver.findElement(By.id("pay")).click();
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        assertEquals(driver.getTitle(), ticketsTitle);
    }

}
