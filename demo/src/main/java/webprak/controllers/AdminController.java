package webprak.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import webprak.DAO.ClientDAO;
import webprak.DAO.RaceDAO;
import webprak.DAO.TicketDAO;
import webprak.DAO.TicketClientDAO;
import webprak.DAO.impl.ClientDAOImpl;
import webprak.DAO.impl.RaceDAOImpl;
import webprak.DAO.impl.TicketClientDAOImpl;
import webprak.DAO.impl.TicketDAOImpl;
import webprak.models.Client;
import webprak.models.Race;
import webprak.models.Ticket;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private final ClientDAO clientDAO = new ClientDAOImpl();
    @Autowired
    private final RaceDAO raceDAO = new RaceDAOImpl();
    @Autowired
    private final TicketDAO ticketDAO = new TicketDAOImpl();
    @Autowired
    private final TicketClientDAO ticketClientDAO = new TicketClientDAOImpl();

    @RequestMapping(value = "/")
    public String root(){
        return "admin/home";
    }

    @RequestMapping(value = "/home")
    public String home(){
        return "admin/home";
    }

    @RequestMapping(value = "/search")
    public String search() { return "admin/search"; }

    @GetMapping("/races")
    public String races(Model model){
        Iterable<Race> races = raceDAO.getAll();
        model.addAttribute("races", races);
        return "admin/races";
    }

    @RequestMapping(value = "/error")
    public String error(){
        return "admin/error";
    }

    @GetMapping(value = "/addRace")
    public String editRace(){
        return "admin/addRace";
    }

    @PostMapping(value = "/saveRace")
    public String saveRace(@RequestParam(name = "raceNumber") Long raceNumber,
                           @RequestParam(name = "companyName") String companyName,
                           @RequestParam(name = "stations") String stations,
                           @RequestParam(name = "costs") String costs,
                           @RequestParam(name = "dates") String dates,
                           @RequestParam(name = "seatsNumber") Long seatsNumber) {
        String[] stationsStrArray = stations.split(";");
        String[] costsStrArray = costs.split(";");
        String[] datesStrArray = dates.split(";");

        Long[] costsLongArray = new Long[costsStrArray.length];
        for (int i = 0; i < costsStrArray.length; i++)
            costsLongArray[i] = Long.parseLong(costsStrArray[i]);

        Long[] datesLongArray = new Long[datesStrArray.length];
        for (int i = 0; i < datesStrArray.length; i++)
            datesLongArray[i] = Long.parseLong(datesStrArray[i]);

        Long dateFrom = datesLongArray[0];
        Long dateTo = datesLongArray[datesLongArray.length - 1];

        Race race = new Race(null, raceNumber, companyName, seatsNumber, stationsStrArray, costsLongArray, datesLongArray, dateFrom, dateTo);

        raceDAO.save(race);
        return "redirect:/admin/races";
    }

    @PostMapping(value = "/removeRace")
    public String removeRace(@RequestParam(name = "raceId") Long raceId){
        raceDAO.delete(raceDAO.getById(raceId));
        return "redirect:/admin/races";
    }

    @RequestMapping(value = "/clients")
    public String clients(Model model){
        Iterable<Client> clients = clientDAO.getAll();
        model.addAttribute("clients", clients);
        model.addAttribute("clientService", ticketClientDAO);
        return "admin/clients";
    }



    @GetMapping(value = "/searchRaces")
    public String searchRaces(@RequestParam(name = "stationFrom") String stationFrom,
                              @RequestParam(name = "stationTo") String stationTo,
                              @RequestParam(name = "date") Long date,
                              Model model) {
        List<Race> rightRaces = new ArrayList<>();
        var races = raceDAO.getAll();
        boolean isFromEquals = false;
        boolean isToEquals = false;
        for(Race race : races){
            for(String station : race.getStations()){
                if(station.equals(stationFrom) && !isFromEquals){
                    isFromEquals = true;
                }
                else if(station.equals(stationTo) && !isToEquals && isFromEquals){
                    isToEquals = true;
                }
            }
            if(isFromEquals && isToEquals){
                rightRaces.add(race);
            }
            isFromEquals = false;
            isToEquals = false;
        }
        model.addAttribute("races", rightRaces);
        return "redirect:/admin/races";
    }
}
