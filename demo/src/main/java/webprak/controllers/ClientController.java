package webprak.controllers;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import webprak.DAO.ClientDAO;
import webprak.DAO.impl.ClientDAOImpl;
import webprak.models.Client;

import java.util.Objects;

@Controller
public class ClientController {
    @Autowired
    private final ClientDAO clientDAO = new ClientDAOImpl();

    @PostMapping("/registrationClient")
    public String savePersonPage(@RequestParam(name = "name") String name,
                                 @RequestParam(name = "surname") String surname,
                                 @RequestParam(name = "patronymic") String patronymic,
                                 @RequestParam(name = "address") String address,
                                 @RequestParam(name = "email") String email,
                                 @RequestParam(name = "password") String password,
                                 Model model) {
        Client client;
        client = new Client(null, name, surname, patronymic, address, "Russia", email, password, false);
        clientDAO.save(client);

        return "redirect:/entry";
    }


}
