package webprak.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import webprak.DAO.RaceDAO;
import webprak.DAO.TicketDAO;
import webprak.DAO.impl.RaceDAOImpl;
import webprak.DAO.impl.TicketDAOImpl;
import webprak.models.Race;

@Controller
public class TicketController {
    @Autowired
    private final TicketDAO ticketDAO = new TicketDAOImpl();

    @Autowired
    private final RaceDAO raceDAO = new RaceDAOImpl();

    @GetMapping("/editPerson")
    public String editPersonPage(@RequestParam(name = "personId", required = false) Long personId, Model model) {
        if (personId == null) {
            model.addAttribute("person", new Race());
            model.addAttribute("personService", raceDAO);
            model.addAttribute("placeService", ticketDAO);
            return "editPerson";
        }

        model.addAttribute("error_msg", "В базе нет человека с ID = " + personId);
        return "error_page";
    }
}
