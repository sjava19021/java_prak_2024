package webprak.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import webprak.DAO.ClientDAO;
import webprak.DAO.RaceDAO;
import webprak.DAO.TicketClientDAO;
import webprak.DAO.TicketDAO;
import webprak.DAO.impl.ClientDAOImpl;
import webprak.DAO.impl.RaceDAOImpl;
import webprak.DAO.impl.TicketClientDAOImpl;
import webprak.DAO.impl.TicketDAOImpl;
import webprak.models.Client;
import webprak.models.Race;
import webprak.models.Ticket;
import webprak.models.TicketClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
public class HomeController {
    @Autowired
    private final ClientDAO clientDAO = new ClientDAOImpl();
    @Autowired
    private final RaceDAO raceDAO = new RaceDAOImpl();
    @Autowired
    private final TicketDAO ticketDAO = new TicketDAOImpl();
    @Autowired
    private final TicketClientDAO ticketClientDAO = new TicketClientDAOImpl();

    private String role = "user"; // {admin, client, user}
    private Client client = null; // initialized if role == user

    @RequestMapping(value = "/")
    public String root(RedirectAttributes redirectAttributes,
                       Model model){
        model.addAttribute("client", client);
        model.addAttribute("role", role);
        return "home";
    }

    @RequestMapping(value = "/home")
    public String home(RedirectAttributes redirectAttributes,
                       Model model) {
        model.addAttribute("client", client);
        model.addAttribute("role", role);
        return "home";
    }

    @RequestMapping(value = "/search")
    public String search(Model model) { return "search"; }

    @RequestMapping(value = "/races")
    public String races(RedirectAttributes redirectAttributes, Model model) {
        Iterable<Race> races = raceDAO.getAll();
        model.addAttribute("races", races);
        model.addAttribute("client", client);
        model.addAttribute("role", role);
        return "races";
    }

    @RequestMapping("/races/{id}")
    public String race(@PathVariable Long id, Model model){
        model.addAttribute("role", role);
        return "races";
    }


    @GetMapping("/registration")
    public String registration(Model model){
        model.addAttribute("role", role);
        return "registration";
    }

    @GetMapping("/entry")
    public String entry(Model model){
        model.addAttribute("role", role);
        return "entry";
    }

    @GetMapping("/findTickets")
    public String findTickets(@RequestParam(name = "stationFrom") String stationFrom,
                              @RequestParam(name = "stationTo") String stationTo,
                              @RequestParam(name = "date") Long date,
                              Model model) {
        Iterable<Race> races = raceDAO.getAll();
        ArrayList<Race> findedRaces = new ArrayList<>() {};
        for(Race race : races){
            String[] raceStations = race.getStations();
            boolean stationFromFound = false;
            boolean stationToFound = false;
            for(String station : raceStations){
                if(station.equals(stationFrom) && !stationFromFound){
                    stationFromFound = true;
                }
                if(station.equals(stationTo) && !stationToFound){
                    stationToFound = true;
                    break;
                }
            }
            if(stationFromFound && stationToFound){
                findedRaces.add(race);
            }
        }
        model.addAttribute("races", findedRaces);
        return "redirect:/races";
    }

    @PostMapping(value = "buyTicket")
    public String buyTicket(@RequestParam(name = "raceId") Long raceId,
                            @RequestParam(name = "stationFrom") String stationFrom,
                            @RequestParam(name = "stationTo") String stationTo,
                            @RequestParam(name = "placeNumber") Long placeNumber,
                            @RequestParam(name = "costs") Long costs,
                            @RequestParam(name = "clientId") Long clientId,
                            RedirectAttributes redirectAttributes,
                            Model model) {
        if(raceDAO.getById(raceId).getSeatsNumber() < placeNumber){
            String errorMsg = "Укажите верный номер места. Доступно мест: " + raceDAO.getById(raceId).getSeatsNumber();
            model.addAttribute("message",errorMsg);
            redirectAttributes.addFlashAttribute("message", errorMsg);
            return "redirect:/error";
        }
        for(var purchasedTicket : ticketDAO.getTicketsByRaceId(raceId)){
            if(Objects.equals(purchasedTicket.getPlaceNumber(), placeNumber)){
                String errorMsg = "Это место уже куплено, выберите другое";
                model.addAttribute("message",errorMsg);
                redirectAttributes.addFlashAttribute("message", errorMsg);
                return "redirect:/error";
            }
        }

        Client client = clientDAO.getById(clientId);
        if(client == null){
            String errorMsg = "Только зарегистрированные пользователи могут покупать билет";
            model.addAttribute("message", errorMsg);
            redirectAttributes.addFlashAttribute("message", errorMsg);
            return "redirect:/error";
        }

        Ticket ticket = new Ticket(null, raceId, 0L, placeNumber, stationFrom, stationTo, costs);
        ticketDAO.save(ticket);


        TicketClient ticketClient = new TicketClient(null, ticket.getId(), clientId);
        ticketClientDAO.save(ticketClient);

        model.addAttribute("client", client);
        model.addAttribute("role", role);
        return "redirect:/tickets";
    }

    @GetMapping(value = "/order")
    public String order(@RequestParam(name = "raceId") Long raceId,
            Model model)  {
        if(Objects.equals(role, "amdin")){
            String errMsg = "Страница покупки билетов не доступна для администратора. Создайте учтную запись пользователя.";
            model.addAttribute("message", errMsg);
            return "redirect:/error";
        } else if (Objects.equals(role, "client")) {
            model.addAttribute("currentRace", raceDAO.getById(raceId));
            model.addAttribute("freePlaces", raceDAO.getFreePlaces(raceId));
            model.addAttribute("client", client);
            return "order";
        } else {
            return "redirect:/registration";
        }
    }


    @GetMapping(value = "/searchRaces")
    public String searchRaces(@RequestParam(name = "stationFrom") String stationFrom,
                              @RequestParam(name = "stationTo") String stationTo,
                              @RequestParam(name = "date") Long date,
                              Model model) {
        List<Race> rightRaces = new ArrayList<>();
        var races = raceDAO.getAll();
        boolean isFromEquals = false;
        boolean isToEquals = false;
        for(Race race : races){
            for(String station : race.getStations()){
                if(station.equals(stationFrom) && !isFromEquals){
                    isFromEquals = true;
                }
                else if(station.equals(stationTo) && !isToEquals && isFromEquals){
                    isToEquals = true;
                }
            }
            if(isFromEquals && isToEquals){
                rightRaces.add(race);
            }
            isFromEquals = false;
            isToEquals = false;
        }
        model.addAttribute("role", role);
        model.addAttribute("races", rightRaces);
        return "redirect:/findTickets";
    }

    @PostMapping("/entryClient")
    public String entryClientPage(@RequestParam(name = "login") String login,
                                  @RequestParam(name = "password") String password,
                                  RedirectAttributes redirectAttributes,
                                  Model model) {
        if(Objects.equals(login, "admin") && Objects.equals(password, "admin")){
            role = "admin";
        } else if (clientDAO.authentication(login, password)) {
            role = "client";
            client = clientDAO.getByEmail(login);
        } else {
            role = "user";
            String errorMsg = "Неправильный логин или пароль";
            model.addAttribute("message", errorMsg);
            redirectAttributes.addFlashAttribute("message", errorMsg);
            return "redirect:/error";
        }
        model.addAttribute("role", role);
        redirectAttributes.addFlashAttribute("role", role);
        model.addAttribute("client", client);
        redirectAttributes.addFlashAttribute("client", client);
        return "redirect:/home";
    }

    @RequestMapping(value = "/exit")
    public String exit(Model model){
        if(Objects.equals(role, "user")){
            model.addAttribute("message", "Эта страница доступна только зарегистрированным пользователям.");
            return "error";
        }

        role = "user";
        client = null;
        return "redirect:/home";
    }

    @RequestMapping(value = "/clients")
    public String clients(Model model){
        if(Objects.equals(role, "admin")) {
            Iterable<Client> clients = clientDAO.getAll();
            model.addAttribute("clients", clients);
            model.addAttribute("clientService", ticketClientDAO);
            model.addAttribute("role", role);
            return "clients";
        } else {
            model.addAttribute("message", "Эта страница доступна только администраторам.");
            return "error";
        }
    }

    @RequestMapping(value = "/tickets")
    public String tickets(Model model){
        model.addAttribute("client", client);
        model.addAttribute("role", role);

        if(Objects.equals(role, "admin")) {
            Iterable<Ticket> tickets = ticketDAO.getAll();
            model.addAttribute("tickets", tickets);
            return "tickets";
        } else if (Objects.equals(role, "client")){
            Long curClientId = client.getId();
            Iterable<Ticket> tickets = ticketClientDAO.getTicketsByClientId(curClientId);
            model.addAttribute("tickets", tickets);
            return "tickets";
        } else {
            model.addAttribute("message", "Эта страница доступна только зарегистрированным пользователям.");
            return "error";
        }
    }

    @GetMapping(value = "/addRace")
    public String editRace(){
        return "addRace";
    }

    @PostMapping(value = "/saveRace")
    public String saveRace(@RequestParam(name = "raceNumber") Long raceNumber,
                           @RequestParam(name = "companyName") String companyName,
                           @RequestParam(name = "stations") String stations,
                           @RequestParam(name = "costs") String costs,
                           @RequestParam(name = "dates") String dates,
                           @RequestParam(name = "seatsNumber") Long seatsNumber) {
        String[] stationsStrArray = stations.split(";");
        String[] costsStrArray = costs.split(";");
        String[] datesStrArray = dates.split(";");

        Long[] costsLongArray = new Long[costsStrArray.length];
        for (int i = 0; i < costsStrArray.length; i++)
            costsLongArray[i] = Long.parseLong(costsStrArray[i]);

        Long[] datesLongArray = new Long[datesStrArray.length];
        for (int i = 0; i < datesStrArray.length; i++)
            datesLongArray[i] = Long.parseLong(datesStrArray[i]);

        Long dateFrom = datesLongArray[0];
        Long dateTo = datesLongArray[datesLongArray.length - 1];

        Race race = new Race(null, raceNumber, companyName, seatsNumber, stationsStrArray, costsLongArray, datesLongArray, dateFrom, dateTo);

        raceDAO.save(race);
        return "redirect:/races";
    }
}
