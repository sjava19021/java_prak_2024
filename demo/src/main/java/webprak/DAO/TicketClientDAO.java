package webprak.DAO;

import webprak.models.Ticket;
import webprak.models.TicketClient;

import java.util.Collection;

public interface TicketClientDAO extends CommonDAO<TicketClient, Long> {
    Collection<Ticket> getTicketsByClientId(Long clientId);
}
