package webprak.DAO.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import webprak.models.Client;
import webprak.DAO.ClientDAO;
import webprak.models.Ticket;

import java.util.List;
import java.util.Objects;

@Repository
public class ClientDAOImpl extends CommonDAOImpl<Client, Long> implements ClientDAO {
    public ClientDAOImpl() {
        super(Client.class);
    }

    @Override
    public Client getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            var clients = getAll();
            for (var client : clients) {
                if(client.getEmail().equals(email)) {
                    return client;
                }
            }
            return null;
        }
    }

    @Override
    public boolean authentication(String email, String password){
        List<Client> clients = (List<Client>) getAll();
        for(var client : clients){
            if(Objects.equals(client.getEmail(), email) && Objects.equals(client.getPasword(), password))
                return true;
        }
        return false;
    }

    @Override
    public boolean isAdmin(String email, String password){
        return authentication(email, password);
    }
}
