package webprak.DAO.impl;

import org.springframework.stereotype.Repository;
import webprak.DAO.TicketDAO;
import webprak.models.Client;
import webprak.models.Ticket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;


@Repository
public class TicketDAOImpl extends CommonDAOImpl<Ticket, Long> implements TicketDAO {
    public TicketDAOImpl(){
        super(Ticket.class);
    }

    @Override
    public Collection<Ticket> getTicketsByRaceId(Long raceId){
        List<Ticket> tickets = (List<Ticket>) getAll();
        List<Ticket> filteredTickets = new ArrayList<>();
        for(var ticket : tickets) {
            if(Objects.equals(ticket.getRaceId(), raceId)){
                filteredTickets.add(ticket);
            }
        }
        return filteredTickets;
    }
}
