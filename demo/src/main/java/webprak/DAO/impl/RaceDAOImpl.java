package webprak.DAO.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import webprak.models.Race;
import webprak.DAO.RaceDAO;
import webprak.models.Ticket;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;


@Repository
public class RaceDAOImpl extends CommonDAOImpl<Race, Long> implements RaceDAO {
    @Autowired
    private final TicketDAOImpl ticketDAO = new TicketDAOImpl();

    public RaceDAOImpl() {
        super(Race.class);
    }

    @Override
    public List<Long> getFreePlaces(Long raceId){
        List<Long> freePlaces = new ArrayList<>();
        Race curRace = getById(raceId);
        if(curRace == null){
            return freePlaces;
        }

        for(Long i = 1L; i < curRace.getSeatsNumber(); ++i){
            freePlaces.add(i);
        }

        List<Ticket> tickets = (List<Ticket>) ticketDAO.getTicketsByRaceId(raceId);
        if(tickets == null){
            return freePlaces;
        }
        for(Ticket ticket : tickets){
            freePlaces.remove(ticket.getPlaceNumber());
        }
        return freePlaces;
    }
}
