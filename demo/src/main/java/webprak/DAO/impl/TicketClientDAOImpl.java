package webprak.DAO.impl;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import webprak.DAO.TicketClientDAO;
import webprak.models.Race;
import webprak.models.Ticket;
import webprak.models.TicketClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class TicketClientDAOImpl extends CommonDAOImpl<TicketClient, Long> implements TicketClientDAO {
    @Autowired
    private final TicketDAOImpl ticketDAOImpl = new TicketDAOImpl();

    public TicketClientDAOImpl() {
        super(TicketClient.class);
    }

    @Override
    public List<Ticket> getTicketsByClientId(Long clientId){
        try(Session session = sessionFactory.openSession()) {
            List<Ticket> tickets = new ArrayList<>();
            for (TicketClient ticketClient : getAll()) {
                if (ticketClient.getClientId().equals(clientId)) {
                    Ticket ticket = ticketDAOImpl.getById(ticketClient.getTicketId());
                    tickets.add(ticket);
                }
            }
            return tickets;
        }
    }
}
