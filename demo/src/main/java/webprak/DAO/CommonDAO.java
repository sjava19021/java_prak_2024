package webprak.DAO;

import webprak.models.CommonEntity;

import java.util.Collection;

public interface CommonDAO<T extends CommonEntity<ID>, ID> {
    T getById(ID id);
    Collection<T> getAll();
    void add(T entity);
    void update(T entity);
    void save(T entity);
    void saveCollection(Collection<T> entities);
    void delete(ID id);
    void delete(T entity);
    boolean isEmpty();
    void clear();
}

