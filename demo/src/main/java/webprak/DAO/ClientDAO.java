package webprak.DAO;

import webprak.models.Client;
import webprak.models.Ticket;

import java.util.List;

public interface ClientDAO extends CommonDAO<Client, Long>{
    Client getByEmail(String email);
    boolean authentication(String email, String password);
    boolean isAdmin(String email, String password);
}
