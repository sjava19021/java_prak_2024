package webprak.DAO;

import webprak.models.Ticket;

import java.util.Collection;


public interface TicketDAO extends CommonDAO<Ticket, Long>{
    Collection<Ticket> getTicketsByRaceId(Long raceId);
}
