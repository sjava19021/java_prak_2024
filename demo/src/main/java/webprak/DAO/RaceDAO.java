package webprak.DAO;

import webprak.models.Race;

import java.util.Collection;

public interface RaceDAO extends CommonDAO<Race, Long>{
    Collection<Long> getFreePlaces(Long raceId);
}
