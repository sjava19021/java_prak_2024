package webprak.models;

import lombok.*;
import org.hibernate.annotations.Type;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "races")
@Getter
@Setter
@ToString
@AllArgsConstructor
//@RequiredArgsConstructor
@NoArgsConstructor
public class Race implements CommonEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "race_id")
    private Long id;

    @Column(name = "race_number")
    private Long raceNumber;

    @Column(name = "company_name")
    private String companyName;

    @Column(nullable = false, name = "seats_number")
    private Long seatsNumber;


    @Column(columnDefinition = "text[]")
    @Type(type = "webprak.PostgreSqlStringArrayType")
    private String[] stations;

    @Column(columnDefinition = "long[]")
    @Type(type = "webprak.PostgreSqlLongArrayType")
    private Long[] costs;

    @Column(columnDefinition = "long[]")
    @Type(type = "webprak.PostgreSqlLongArrayType")
    private Long[] time;

    @Column(nullable = false, name = "dateFrom")
    private Long dateFrom;

    @Column(nullable = false, name = "dateTo")
    private Long dateTo;

    public String getStationFrom(){
        return getStations()[0];
    }

    public String getSationTo(){
        int stationCount = getStations().length;
        return getStations()[stationCount-1];
    }

    public Long getSummaryCost(){
        int costsCount = getCosts().length;
        return getCosts()[costsCount-1];
    }

    public String[] getInternalStations(){
        String[] stations = getStations();
        List<String> internalStations = new ArrayList<>(Arrays.asList(stations));
        internalStations.remove(stations[0]);
        internalStations.remove(stations[stations.length-1]);
        stations = internalStations.toArray(new String[internalStations.size()]);
        return stations;
    }

    public Long getCosts(String stationFrom, String stationTo){
        Long leftBound = -1L , rightBound = -1L;
        Long[] costs = getCosts();
        String[] stations = getStations();
        for(int i = 0; i < stations.length; i++){
            if(stations[i].equals(stationFrom) && leftBound == -1L){
                leftBound = costs[i];
            } else if(stations[i].equals(stationTo) && rightBound == -1L){
                rightBound = costs[i];
            }
        }
        return rightBound - leftBound;
    }
}


