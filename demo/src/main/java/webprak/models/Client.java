package webprak.models;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;
/*
*                       client_id SERIAL PRIMARY KEY,
                        name varchar(40) NOT NULL ,
                        surname varchar(40) NOT NULL ,
                        patronymic varchar(40),
                        address varchar(40),
                        email varchar(40)
* */
@Entity
@Table(name="clients")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class Client implements CommonEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "client_id")
    private Long id;

    @Column(nullable = false, name = "name")
    @NonNull
    private String name;

    @Column(nullable = false, name = "surname")
    @NonNull
    private String surname;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "address")
    private String address;

    @Column(name = "country")
    private String country;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String pasword;

    @Column(name = "isAdmin")
    private boolean isAdmin;
}
