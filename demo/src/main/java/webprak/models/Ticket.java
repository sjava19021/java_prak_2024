package webprak.models;

import lombok.*;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tickets")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Ticket implements CommonEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "ticket_id")
    private Long id;

    @Column(nullable = false, name = "race_id")
    private Long raceId;

    @Column(name = "date")
    private Long date;

    @Column(name = "place_number")
    private Long placeNumber;

    @Column(name = "station_from")
    private String stationFrom;

    @Column(name = "station_to")
    private String stationTo;

    @Column(name = "costs")
    private Long costs;
}
