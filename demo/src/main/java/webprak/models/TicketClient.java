package webprak.models;

import lombok.*;

import javax.persistence.*;
@Entity
@Table(name = "tickets_clients")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TicketClient implements CommonEntity<Long>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "ticket_client_id")
    private Long id;

    @Column(name = "ticket_id")
    private Long ticketId;

    @Column(name = "client_id")
    private Long clientId;
}
