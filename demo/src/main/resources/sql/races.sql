DROP TABLE IF EXISTS races CASCADE;

CREATE TABLE races(
        race_id SERIAL PRIMARY KEY,
        race_number integer CHECK(race_number > 0),
        company_name varchar(40),
        seats_number integer CHECK (seats_number > 0),
        stations text[],
        costs bigint[],
        time bigint[]
);