DROP TABLE IF EXISTS tickets_clients CASCADE;

CREATE TABLE tickets_clients(
        ticket_client_id SERIAL PRIMARY KEY,
        ticket_id integer REFERENCES tickets(ticket_id),
        client_id integer REFERENCES clients(client_id)
);