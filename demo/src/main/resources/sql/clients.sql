DROP TABLE IF EXISTS clients CASCADE;

CREATE TABLE clients(
                        client_id SERIAL PRIMARY KEY,
                        name varchar(40) NOT NULL ,
                        surname varchar(40) NOT NULL ,
                        patronymic varchar(40),
                        address varchar(40),
                        country varchar(40),
                        email varchar(40),
                        password varchar(40),
                        isAdmin boolean
);
