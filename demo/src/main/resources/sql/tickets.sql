DROP TABLE IF EXISTS tickets CASCADE;

CREATE TABLE tickets(
        ticket_id SERIAL PRIMARY KEY,
        race_id integer REFERENCES races(race_id),
        date bigint,
        place_number integer CHECK(place_number > 0),
        station_from varchar(40) NOT NULL,
        station_to varchar(40) NOT NULL
);